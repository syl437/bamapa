// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova','google.places','rzModule'])

.run(function($ionicPlatform,$rootScope) {
	
	$rootScope.Host = 'https://tapper.co.il/bamapa/laravel/public/';
	$rootScope.PHPHost = 'https://tapper.co.il/bamapa/php/';
	$rootScope.SuppliersData = [];
	$rootScope.DefaultCatagory = 4;
	$rootScope.DefaultRadius = 4;
	$rootScope.selectedIndex = 0;
	
  $ionicPlatform.ready(function() {
	  
	  
  var notificationOpenedCallback = function(jsonData) {
	  //alert (JSON.stringify(jsonData));
	  //alert (jsonData.additionalData.type);
	 
	  if (jsonData.additionalData.type == "rate")
	  {
		  $rootScope.pushRedirect = jsonData.additionalData.id;
		  
			if ($rootScope.pushRedirect)
			{
				window.location ="#/app/ratesupplier/"+$rootScope.pushRedirect;
				$rootScope.pushRedirect = "";
			}
	  }
	  
	  
    console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
  };

  window.plugins.OneSignal.init("72aa1c8a-fa13-4a50-be65-fa170a266b62",
                                 {googleProjectNumber: "685748499922"},
                                 notificationOpenedCallback);

  window.plugins.OneSignal.getIds(function(ids) {
	
  $rootScope.pushId = ids.userId;	
  


	  
  
  //$scope.pushId = ids.userId;
	
	
   //alert (ids.userId)
   //alert (ids.pushToken);

				
  //console.log('getIds: ' + JSON.stringify(ids));
});

								 
  // Show an alert box if a notification comes in when the user is in your app.
  window.plugins.OneSignal.enableInAppAlertNotification(true);



  
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	
$ionicConfigProvider.backButton.previousTitleText(false).text('');
	
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

    .state('app.loginoptions', {
      url: '/loginoptions',
      views: {
        'menuContent': {
          templateUrl: 'templates/loginoptions.html',
          controller: 'LoginOptionsCtrl'
        }
      }
    })



    .state('app.register', {
      url: '/register',
      views: {
        'menuContent': {
          templateUrl: 'templates/register.html',
          controller: 'RegisterCtrl'
        }
      }
    })
	
	.state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })
	
	
    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })
    .state('app.provider', {
      url: '/provider',
      views: {
        'menuContent': {
          templateUrl: 'templates/provider.html',
          controller: 'ProviderCtrl'
        }
      }
    })
	
    .state('app.info', {
      url: '/info/:CatagoryIndex/:SupplierIndex',
      views: {
        'menuContent': {
          templateUrl: 'templates/info.html',
          controller: 'InfoCtrl'
        }
      }
    })

	
    .state('app.terms', {
      url: '/terms',
      views: {
        'menuContent': {
          templateUrl: 'templates/terms.html',
          controller: 'TermsCtrl'
        }
      }
    })

    .state('app.about', {
      url: '/about',
      views: {
        'menuContent': {
          templateUrl: 'templates/about.html',
          controller: 'AboutCtrl'
        }
      }
    })
	
    .state('app.how', {
      url: '/how',
      views: {
        'menuContent': {
          templateUrl: 'templates/how.html',
          controller: 'HowCtrl'
        }
      }
    })

	
    .state('app.working', {
      url: '/working',
      views: {
        'menuContent': {
          templateUrl: 'templates/working.html',
          controller: 'WorkingDaysCtrl'
        }
      }
    })

	
	
    .state('app.contact', {
      url: '/contact',
      views: {
        'menuContent': {
          templateUrl: 'templates/contact.html',
          controller: 'ContactCtrl'
        }
      }
    })

	
	
	
    .state('app.myprofile', {
      url: '/myprofile',
      views: {
        'menuContent': {
          templateUrl: 'templates/myprofile.html',
          controller: 'MyProfileCtrl'
        }
      }
    })	
	
    .state('app.available', {
      url: '/available',
      views: {
        'menuContent': {
          templateUrl: 'templates/available.html',
          controller: 'AvailableCtrl'
        }
      }
    })
	
    .state('app.intro', {
      url: '/intro',
      views: {
        'menuContent': {
          templateUrl: 'templates/intro.html',
          controller: 'IntroCtrl'
        }
      }
    })
	
    .state('app.editprofile', {
      url: '/editprofile',
      views: {
        'menuContent': {
          templateUrl: 'templates/editprofile.html',
          controller: 'EditProfileCtrl'
        }
      }
    })


    .state('app.forgotpass', {
      url: '/forgotpass',
      views: {
        'menuContent': {
          templateUrl: 'templates/forgotpass.html',
          controller: 'ForgotPassCtrl'
        }
      }
    })

	
    .state('app.ratesupplier', {
      url: '/ratesupplier/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/ratesupplier.html',
          controller: 'RateSupplierCtrl'
        }
      }
    })

	
	
;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/loginoptions');
});
